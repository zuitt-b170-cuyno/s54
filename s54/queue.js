let collection = [];

// Write the queue functions below.
// To test your skills, try to use pure javascript coding and avoid using pop, push, slice, splice, etc.

function print() {
	// output all the elements of the queue
    return collection
}

function enqueue(element) {
	// add element to rear of queue
    const length = collection.length
    collection[length] = element
    return collection
}

function dequeue() {
	// remove element at front of queue
    const tmp = collection, newCollection = []
    for (let i = 1; i < collection.length; i++)
        newCollection[i - 1] = tmp[i]
    collection = newCollection

    return collection
}

function front() {
	// show element at the front
    return collection[0]
}

function size() {
    // show total number of elements
    return collection.length
}

function isEmpty() {
    // outputs Boolean value describing whether queue is empty or not
    return !collection.length
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
